using HellowWorldApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace HellowWorldApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    //LocalHost:3001/WeatherForecast
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[] {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        /* private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        } */

        [HttpGet]
        [Route("GetWeatherForecast")]
        // LocalHost:3001/WeatherForecast/GetWeatherForecast
        public IEnumerable<WeatherForecast> Get()
        {
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet]
        [Route("GetByTemp/{temp}")]
        // LocalHost:3001/WeatherForecast/GetByTemp/{temp}
        public WeatherForecast GetOne([FromRoute] int temp)
        {
            return new WeatherForecast
            {
                Date = DateTime.Now,
                TemperatureC = temp,
                Summary = "Hola mundo!"
            };
        }

        [HttpGet]
        [Route("GetOne")]
        // LocalHost:3001/WeatherForecast/GetOne?temp=30&summary=Hola!
        public WeatherForecast GetOne([FromQuery] int temp, [FromQuery] string? summary = "default")
        {
            if (temp > 30)
            {
                summary = "Hace calor!";
            } else if (temp < 10)
            {
                summary = "Hace frio!";
            }
            return new WeatherForecast
            {
                Date = DateTime.Now,
                TemperatureC = temp,
                Summary = summary
            };
        }
    }
}