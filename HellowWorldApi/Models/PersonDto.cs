﻿namespace HellowWorldApi.Models
{
    public class PersonDto
    {
        public string Speak{ get; set; }
        public string Name { get; set; }
        public int Edad { get; set; }
        public double? Estatura { get; set; }
    }
}

