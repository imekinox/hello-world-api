﻿namespace HellowWorldApi.Models
{
    public class PersonPatchDto
    {
        public string Name { get; set; }
        public string Speak { get; set; }
    }
}
